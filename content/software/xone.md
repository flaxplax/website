---
title: "Xone"
date: 2022-04-18T10:05:16+02:00
lastmod: 2022-04-18T10:05:16+02:00
author: "Flaxplax"
description: "xone documentation"
draft: false
---

## [xone](https://github.com/medusalix/xone)
It's software for xbox controller to work in linux

### installation
```bash
# clone
git clone https://github.com/medusalix/xone
# cd and install
cd xone
sudo ./install.sh --release
# download the firmware for the dongle
sudo xone-get-firmware.sh
# plug in the dongle/controller
```
### updating
```bash
# uninstall it
sudo ./uninstall.sh
# go back and install it again
```