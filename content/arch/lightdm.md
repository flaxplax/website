---
title: "LightDM"
date: 2020-07-09T20:52:15+02:00
lastmod: 2020-07-09T20:52:15+02:00
author: "Flaxplax"
draft: false
description: "Arch lightdm documentation"
---

## Install LightDM

[LightDM](https://wiki.archlinux.org/index.php/LightDM) is a Login manager.

You need Xorg for it to work.

```bash
  sudo pacman -S xorg
```

chose all when you install it.


LightDM requiers a greeter to work.

Easiest is to install it with a default greeter.

```bash
  sudo pacman -S lightdm-gtk-greeter
```

enable the service for it to run at startup.

```bash
  sudo systemctl enable lightdm.service
```

This is how it looks like

![lightdm](/images/WM/lightdm.webp)