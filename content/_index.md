---
title: "Home"
date: 2020-07-07T22:38:01+02:00
draft: false
description: "Flaxplax Home Page"
---

## Welcome to my website

Here you will find my personal documentation of things that i have learned throughout the years.

If something is wrong or outdated please email me [website@flaxplax.com](mailto:website@flaxplax.com)

![meme](/images/meme/attention.webp)
