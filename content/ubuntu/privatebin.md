---
title: "Privatebin"
date: 2021-09-16T20:55:53+02:00
lastmod: 2021-09-16T20:55:53+02:00
author: "Flaxplax"
description: "ubuntu privatebin installation"
draft: false
---

## Installation

Update and install git

```bash
apt update
apt upgrade -y
apt install -y git 
```

Install the web server stack

```bash
apt update
apt install -y apache2 php php-xml php-mbstring php-mysql php-json php-pdo 
```

Make sure apache is installed and running

```bash
systemctl enable --now apache2.service 
```

Configure apache

```bash
vim /etc/apache2/sites-available/privatebin.conf
```

Change 'YOUR_SERVER_IP' to you ip address or domain name

```bash
<VirtualHost *:80>
ServerName YOUR_SERVER_IP
DocumentRoot /var/www/html/PrivateBin/
ErrorLog ${APACHE_LOG_DIR}/privatebin-error.log
CustomLog ${APACHE_LOG_DIR}/privatebin-access.log combined
<Directory /var/www/html/PrivateBin>
AllowOverride All
</Directory>
</VirtualHost>
```

Save exit and enable the virtual host then reload the configuration.

```bash
a2ensite privatebin.conf
systemctl reload apache2.service
```

Clone the repository
```bash
cd /var/www/html/ && git clone https://github.com/PrivateBin/PrivateBin.git
```

Give the apache user owenership of the directory
```bash
chown -R www-data:www-data PrivateBin/
```