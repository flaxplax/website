---
title: 'Unifi UAP'
date: '2024-09-26T22:30:27+02:00'
lastmod: '2024-09-26T22:30:27+02:00'
author: "Flaxplax"
description: "Ubiquiti unifi uap ac lite, ap, access point"
draft: false
---

# Unifi UAP

## Show AP info

You can you use the `info` command to show info about the access point

```bash
unifi-BZ.6.6.77# info

Model:       UAP-AC-Lite
Version:     6.6.77.15402
MAC Address: 89:2E:AA:47:8B:A2
IP Address:  192.168.1.223
Hostname:    unifi
Uptime:      4000476 seconds
NTP:         Synchronized

Status:      Connected (http://unifi:8080/inform)
```

## Not showing up in controller/Is offline but still working

Sometimes the AP appear to be offline or it does not show up for adoption, in that case you are able to ssh into the AP and run a command to fix it.

If the ap is not adopted you can connect to it with `ubnt/ubnt` as default username and password.

If the AP is already adopted you can find the password and username in the controller by going to `Settings > System > Advanced` and find username and password under `Device Authentication`

```bash
ssh ubnt@<ip/hostname>
```

Inside the shell of the AP you can run the `set-inform` command to tell the device where the controller is.

```bash
set-inform http://<ip/hostname of controller>:8080/inform
```

After you have run the command the AP should show up in the controller ready for adoption, or be online again if it was showing as offline.
