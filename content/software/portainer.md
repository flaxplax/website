---
title: "Portainer"
date: 2021-03-18T21:04:45+01:00
lastmod: 2021-03-18T21:04:45+01:00
author: "Flaxplax"
description: "portainer documentation"
draft: false
---

## Portainer

Portainer run on docker, you need to [install docker](/software/docker/#install) before you can install portainer

### Install Portainer
```bash
sudo docker run -d \
--name="portainer" \
--restart on-failure \
-p 9000:9000 \
-v /var/run/docker.sock:/var/run/docker.sock \
-v portainer_data:/data \
portainer/portainer-ce
```