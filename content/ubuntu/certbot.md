---
title: "Certbot"
date: 2021-08-27T21:31:48+02:00
lastmod: 2021-08-27T21:31:48+02:00
author: "Flaxplax"
description: "ubuntu certbot documentation"
draft: false
---

## Certbot

### Installation

Make sure that snap are up to date
```bash
sudo snap install core: sudo snap refresh core
```

Install certbot with snap
```bash
sudo snap install --classic certbot
```

Then prepare the certbot command
```bash
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```

### Commands

Run certbot with:
```bash
sudo certbot
```

Make sure to test automatic renewal
```bash
sudo certbot renew --dry-run
```