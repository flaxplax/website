---
title: "Piper"
date: 2021-11-07T20:22:30+01:00
lastmod: 2021-11-07T20:22:30+01:00
author: "Flaxplax"
description: "piper documentation"
draft: false
---

## Piper

[Piper](https://github.com/libratbag/piper/wiki/Installation) is a software to configure gaming mouse

It requires [libratbag](https://github.com/libratbag/libratbag/wiki/Installation)

### Installation

Fedora installation
```bash
sudo dnf install libratbag-ratbagd
sudo dnf install piper
```

Arch installation
```bash
sudo pacman -S libratbag
sudo pacman -S piper
```
Debian/Ubuntu installation
```bash
sudo apt install ratbagd
sudo apt install piper
```
