---
title: "Mplayer"
date: 2020-07-12T20:56:04+02:00
lastmod: 2020-07-12T20:56:04+02:00
author: "Flaxplax"
description: "mplayer documentation"
draft: false
---

[Mplayer](https://wiki.archlinux.org/index.php/MPlayer) is a video player that supports multiple video formats.

## Install

```bash
Arch:
  sudo pacman -S mplayer
debian/ubuntu
  sudo apt install mplayer
```

### usage

To start a video type:

```bash
  mplayer $path
```

Simple keybindings.

```bash
  f #for full-screen
  q #to quit
  0 #for volume up
  9 #for volume down
  right #forward 10 seconds
  left #backwards 10 seconds
  up #forward 1 minute
  down #backward 1 minute
  p/space #pause, press again for unpause
```
