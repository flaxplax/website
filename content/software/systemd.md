---
title: "Systemd"
date: 2021-12-25T19:54:57+01:00
lastmod: 2024-04-12T20:00:00+01:00
author: "Flaxplax"
description: "systemd documentation"
draft: false
---

## Simple service

To create a simple service. First enter system's service folder.
```bash
cd /lib/systemd/system
```
Create a file named after you service
```bash
touch <your-service-name>.service
```
Template for service
```bash
[Unit]
Description=<description about this service>

[Service]
User=<user e.g. root>
WorkingDirectory=<directory_of_script e.g. /root>
ExecStart=<script which needs to be executed>
Restart=always

[Install]
WantedBy=multi-user.target
```
After you added your service file you may need to reload systemd-daemon
```bash
systemctl daemon-reload
```
After the daemon has been reloaded you can start your service
```bash
systemctl start <your-service-name>.service
```
If you want your service to start on boot you can enable it
```bash
systemctl enable <your-service-name>.service
```
