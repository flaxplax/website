---
title: "BTRFS"
date: 2022-08-21T13:40:06+02:00
lastmod: 2022-08-21T13:40:06+02:00
author: "Flaxplax"
draft: false
description: "btrfs documentation"
---

## Create Swapfile

To create a swapfile on btrfs filesystem.

Only works on kernel 5.0 or higher.
```bash
# truncate -s 0 /swapfile
# chattr +C /swapfile
# btrfs property set /swapfile compression none
# fallocate -l 512M /swapfile
# chmod 600 /swapfile
# mkswap /swapfile
```