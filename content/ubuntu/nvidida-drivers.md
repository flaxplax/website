---
title: "Nvidia Drivers"
date: 2022-06-24T13:09:16+02:00
lastmod: 2022-06-24T13:09:16+02:00
author: "Flaxplax"
description: "ubuntu nvidia drivers documentation"
draft: false
---

## Nvidia drivers for ubuntu

first add ppa repo
```bash
sudo add-apt-repository ppa:graphics-drivers/ppa
```
Identify device
```bash
ubuntu-drivers devices
```
Install drivers
```bash
sudo apt install [driver_name]
```