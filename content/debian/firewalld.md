---
title: 'Firewalld'
date: '2024-04-21T19:40:33+02:00'
lastmod: '2024-04-21T19:40:33+02:00'
author: "Flaxplax"
description: "debian firewalld documentation"
draft: false
---

## Firewalld

Firewalld is a linux firewall application.

### Installation

You can easily install firewalld through apt

```bash
apt install firewalld
```

### Zones

You can list zones with the following command

```bash
firewall-cmd --get-zones
block dmz drop external home internal public trusted work
```

You list what services are associated what a zone, if you don't speceified what zone it will query the default zone

```bash
firewall-cmd --list-all
public
  target: default
  icmp-block-inversion: no
  interfaces: 
  sources: 
  services: dhcpv6-client ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

You can also specify the zone

```bash
firewall-cmd --zone=external --list-all
external
  target: default
  icmp-block-inversion: no
  interfaces: 
  sources: 
  services: ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: yes
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules:
```

### Allow and deny port

You can allow a port with the following command.

```bash
firewall-cmd --add-port=2252/udp
```

You can allow it permanently by adding `--permanent` as follows.

```bash
firewall-cmd --permanent --add-port=2252/udp
```

If you want to remove it later you can do it with the following command.

```bash
firewall-cmd --permanent --remove-port=2252/udp
```