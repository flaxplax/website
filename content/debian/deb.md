---
title: 'Deb'
date: '2024-05-04T12:44:03+02:00'
lastmod: '2024-05-04T12:44:03+02:00'
author: "Flaxplax"
description: "Guide: how to create debian deb file"
draft: false
---

## How to create a debian deb file

We can make a simple script and save it as `hello-world.sh`.

```bash
#!/bin/bash

echo "Hello World!"
```

Then you we can create a directory and call it `hello-world`, and create the directory where we want the script to be installed, and move the script there.

```bash
mkdir -p hello-world/usr/bin
mv hello-world.sh hello-world/usr/bin/
```

After we need to create the `control` file

```bash
mkdir -p hello-world/DEBIAN
touch hello-world/DEBIAN/control
cat <<EOT >> hello-world/DEBIAN/control
Package: hello-world
Version: 1.0.0
Maintainer: Flaxplax
Architecture: amd64
Section: misc
Priority: optional
Description: Hello world script
EOT
```

Now we will change permissions on the directory and build the deb package

```bash
chmod 755 -R hello-world
dpkg-deb --build hello-world
```

We can now install the deb package and run the program

```bash
dpkg -i hello-world.deb

hello-world.sh
Hello World!
```
