---
title: "DD"
date: 2022-03-20T10:22:47+01:00
lastmod: 2022-03-20T10:22:47+01:00
author: "Flaxplax"
draft: false
description: "dd documentation"
---

## Create bootable usb

Create a bootable usb with following command:
```bash
dd if=artful-desktop-amd64.iso of=/dev/sdd bs=1M status=progress
```