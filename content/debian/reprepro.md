---
title: 'Reprepro'
date: '2024-04-21T13:22:12+02:00'
lastmod: '2024-04-27T13:22:12+02:00'
author: "Flaxplax"
description: "debian reprepro documentation"
draft: false
---

## Reprepro

reprepo is a APT repository.

This page is a guide on how to install and configure reprepro, and serve it with nginx, on debian bookworm.

### Install packages

First you need to install reprepro, gnupg and nginx.

```bash
apt install reprepro gnupg nginx
```

### Configure the repository

We need to create a gpg key that will be used to sign the repository.

```bash
gpg --gen-key
```

After that we can use `/opt/repo` as the root directory for out repository, we also need a `conf` directory inside that.

```bash
mkdir -p /opt/repo/conf
```

We need our public gpg key that we created earlier.

```bash
gpg --list-keys

/root/.gnupg/pubring.kbx
------------------------
pub   rsa3072 2024-04-21 [SC] [expires: 2026-04-21]
      0548D40EDE49CF56652D065F056751CC91063223
uid           [ultimate] user1 <user1@example.com>
sub   rsa3072 2024-04-21 [E] [expires: 2026-04-21]
```

Our public gpg key is `0548D40EDE49CF56652D065F056751CC91063223`, that we will use from now on.

Now we will create our `/opt/repo/conf/distributions` file and edit it as follows.

```bash
vim /opt/repo/conf/distributions
```

- Origin: This field is a identifier explaining where the repository came from.
- Label: This field is copies to the generated metadata.
- Codename: This field is used to determine the directory structure, chose a Debian or Ubuntu codename for this one.
- Architectures: This field is the names of the components that packages can be imported into.
- Description: This field will be copied into the generated metada, its an optional field.
- SignWith: This field should be your public gpg key, to determine the gpg key the repository will be signed with

```bash
Origin: repo.example.com
Label: repo.example.com
Codename: bookworm
Architectures: amd64
Components: main
Description: example repo
SignWith: 0548D40EDE49CF56652D065F056751CC91063223
```

Now we can import our deb packages into our repository.

```bash
cd /opt/repo
reprepo includedeb bookworm /usr/src/package/*.deb
```

### Configure nginx

First we will configure a vhost for our repository

```bash
vim /etc/nginx/sites-available/repo.example.com
```
```bash
server {
  listen 80;

  server_name repo.example.com;

  location / {
    root /var/packages;
    index index.html;
    autoindex on;
  }

  location ~ /(.*)/conf {
    deny all;
  }

  location ~ /(.*)/db {
    deny all;
  }
}
```

After we will enable the vhost and reload nginx

```bash
ln -s /etc/nginx/sites-available/repo.example.com /etc/nginx/sites-enabled
systemctl reload nginx
```

Now we will export our pgp key for out repository

```bash
gpg --armor --output /opt/repo/repo.example.com.gpg.key --export 0548D40EDE49CF56652D065F056751CC91063223
```

### Using the repository

First we will import our gpg key

```bash
mkdir -p /etc/apt/keyrings
wget http://repo.example.com/repo.example.com.gpg.key
```

Then we will add our repository to a new source list

```bash
cd /etc/apt/sources.list.d
vim repo.example.com 
```
```bash
deb [signed-by=/etc/apt/keyrings/repo.example.com.gpg.key] http://repo.example.com bookworm main
```

Now we can install packages from our repository

```bash
apt update && apt install my-package
