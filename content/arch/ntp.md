---
title: "NTP"
date: 2020-09-03T15:12:21+02:00
lastmod: 2024-05-04T15:12:21+02:00
author: "Flaxplax"
draft: false
description: "Arch ntp documentation"
---

## Network Time Protocol

this will sync you system clock with the Internet.

### systemd-timesyncd

systemd-timesyncd is a systemd ntp client

Start and enable with following command

```bash
  sudo systemctl enable --now systemd-timesyncd
```

### NTPD

If you want to use ntpd install with following command:

```bash
  sudo pacman -S ntp
```

Then you have to start and enable it:

```bash
  sudo systemctl enable ntpd
  sudo systemctl start ntpd
```
