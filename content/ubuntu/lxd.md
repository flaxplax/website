---
title: "LXD"
date: 2021-08-28T09:59:27+02:00
lastmod: 2022-10-09T15:49:27+02:00
Author: "Flaxplax"
description: "ubuntu lxd documentation"
draft: false
---

## Installation
You can install lxd from the repository
```bash
apt install lxd
```

You can also install lxd from snap
```bash
snap install lxd --channel=4.0/stable
```

Then add the user to lxd group
```bash
adduser $USER lxd
```

Apply it by using the newgrp command
```bash
newgrp lxd
```

set up lxd with init
```bash
lxd init
```

## nfs
NFS server need to be a privileged container to run
```bash
lxc config set <container> security.privileged true
```
You also need apparmor to allow nfs stuff
```bash
lxc config set <container> raw.apparmor 'mount fstype=nfs*, mount fstype=rpc_pipefs,'
```
Install nfs-kernel-server
```bash
apt install nfs-kernel-server
```
Check and see if it is running
```bash
systemctl status nfs-kernel-server

● nfs-server.service - NFS server and services
     Loaded: loaded (/lib/systemd/system/nfs-server.service; enabled; vendor preset: enabled)
     Active: active (exited) since Sun 2022-10-09 14:01:28 UTC; 1min 11s ago
   Main PID: 2427 (code=exited, status=0/SUCCESS)

Oct 09 14:01:27 nfs systemd[1]: Starting NFS server and services...
Oct 09 14:01:27 nfs exportfs[2426]: exportfs: can't open /etc/exports for reading
Oct 09 14:01:28 nfs systemd[1]: Finished NFS server and services.
```

## How to add a folder to your lxc container
```bash
lxc config device add $container foldername disk source=$path path=$path
```

## Move container/rename
Rename container
```bash
lxc stop 'container'
lxc move 'container' 'new container name'
lxc start 'new container name'
```

move container to another storage pool
```bash
lxc stop 'container'
lxc move 'container' 'tmp container name' -s <pool>
lxc move 'tmp container name' 'container'
lxc start 'container'
```

## Pass through gpu
set the device to the container
```bash
lxc config device add $container gpu gpu gputype=physical
```
Integrate nvidia drivers
```bash
lxc config set $container nvidia.runtime=true
```