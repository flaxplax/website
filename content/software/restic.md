---
title: "Restic"
date: 2022-01-08T23:40:07+01:00
lastmod: 2022-01-08T23:40:07+01:00
author: "Flaxplax"
description: "restic documentation"
draft: false
---

## Restic

[Restic](https://restic.net/) is a backup tool

### Installation

Alpine
```bash
apk add restic
```
Arch
```bash
pacman -S restic
```
Debian
```bash
apt install restic
```

### Create repo for backup

Local repo
```bash
restic init --repo <path to repo>
```
Enter a password for your repo

**don't forget the password**

With sftp

First set up ssh keys for passwordless ssh. It will not work without ssh keys
```bash
restic -r sftp:user@host:<path to repo> init
```
Enter a password for your repo

**don't forget the password**

### Backup

How to backup
```bash
restic -r <path to repo> --verbose backup <path to folder/file>
```
It is possible to run this command without the ```--verbose``` flag. It will only show less output.

List backups
```bash
restic -r <path to repo> snapshots
```
Filter with ```--path``` flag
```bash
restic -r <path to repo> snapshots --path="<path to folder/file>"
```

### Restore

Restore from snapshot
```bash
restic -r <path to repo> restore <snapshot id> --target <target path>
```

### Remove snapshots

Remove single snapshot
```bash
restic -r <path to repo> forget <snapshot id>
restic -r <path to repo> prune
```
You use prune to delete data after snapshot has been removed.