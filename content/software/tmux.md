---
title: "Tmux"
date: 2021-03-25T18:37:18+01:00
lastmod: 2021-03-25T18:37:18+01:00
author: "Flaxplax"
description: "tmux documentation"
draft: false
---

## TMUX

To enter tmux
```bash
tmux
```
To reattach
```bash
tmux attach
```
Create session with name
```bash
tmux new -s <name>
```
List sessions
```bash
tmux ls
```
Enter specifix session
```bash
tmux attach -t <name>
```