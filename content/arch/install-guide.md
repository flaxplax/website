---
title: "Install"
date: 2020-07-09T18:31:12+02:00
lastmod: 2020-07-09T18:31:12+02:00
draft: false
author: "Flaxplax"
description: "Arch install guide"
---

## Arch Installation

You can find all the information you need to install in the Wiki [here](https://wiki.archlinux.org/index.php/Installation_guide).

You will need the latest ISO. You can find it [here](https://www.archlinux.org/download/).

I'm doing this in a virtual machine in virtualbox. It has 15G of space 2G of ram and 2 cpu cores.

When you boot up Arch you will get a Terminal and all you see is:
```bash
  root@arhciso~#
```
### Set keyboard layout
change the keyboard layout with following command.

```bash
  loadkeys sv-latin1
```

"sv-latin1" is for a Swedish layout. You have to find your layout

Later you should try ping "8.8.8.8" to see if you have a Internet connection. Do that by typing "ping 8.8.8.8"



```bash
  ping 8.8.8.8
  PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
  64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=17.6 ms
  64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=17.5 ms
  64 bytes from 8.8.8.8: icmp_seq=3 ttl=63 time=17.6 ms
  64 bytes from 8.8.8.8: icmp_seq=4 ttl=63 time=17.8 ms
```

If your output looks like this, you have internet.

You can stop the ping by pressing "Ctrl+c"

After the ping you need to update the system clock:

```bash
  timedate set-ntp true
```

### Configure disk partition

Run "lsblk" to see Your disk/disks.

```bash
  lsblk
  loop0 7:0 0 529.7M  1 loop0/run/archiso/sfs/airootfs
  sda   8:0 0    15G  0 disk
```

Here we can see that the disk is named "sda" and has 15G of space

The wiki recomends that you use fdisk but we're going to use cfdisk

```bash
  cfdisk
```
Chose "dos" as label type

![cfdisk](/images/archInstall/cfdisk1.webp)

press enter om "New"

![cfdisk](/images/archInstall/cfdisk2.webp)

Make the partittion size 13G and press eneter

![cfdisk](/images/archInstall/cfdisk3.webp)

Chose as "primary"

![cfdisk](/images/archInstall/cfdisk4.webp)

Go to "Free space" and press enter.

![cfdisk](/images/archInstall/cfdisk5.webp)

Make the second partition the last 2G of space

![cfdisk](/images/archInstall/cfdisk6.webp)

Chose this as "Primary"

![cfdisk](/images/archInstall/cfdisk7.webp)

Chose /dev/sda2 and go to "Type"

![cfdisk](/images/archInstall/cfdisk8.webp)

Select "82 Linux Swap"

![cfdisk](/images/archInstall/cfdisk9.webp)

Go to "Write" press enter end type "yes" for it to make changes and then "Quit"

![cfdisk](/images/archInstall/cfdisk10.webp)

If you type "lsblk" again you can see your 2 partitions you have just made.

```bash
  lsblk
  loop0 7:0 0 529.7M  1 loop0/run/archiso/sfs/airootfs
  sda   8:0 0    15G  0 disk
  |-sda1
  |-sda2
```

after this you have to format your first partition and make the second one a swap drive.

```bash
  mkfs.ext4 /dev/sda1
  mkswap    /dev/sda2
  swapon    /dev/sda2
```

Then you're going to mount the first partition.

```bash
  mount /dev/sda1 /mnt
```

### Base installation

To install Arch base package run following command.

```bash
  pacstrap /mnt base linux linux-firmware
```

After the installation you will generate an fstab.

```bash
  genfstab -U /mnt >> /mnt/etc/fstab
```

change root into the new system

```bash
  arch-chroot /mnt
```

now you are setting the correct time zone
```bash
  ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
```

run hwclock to gen adjtime

```bash
  hwclock --systohc
```

run locale-gen

```bash
  locale-gen
```

now you will install an editor

```bash
  pacman -S vim
```

Press Y and then enter to install.

Use vim to create and edit the locale.conf.

Press " i " to insert text and escape to stop.

```bash
  vim /etc/locale.conf
  LANG=en_US.UTF-8
  :wq!
```

" :wq! " is what you use to save and exit the editor.

Use vim to edit your keyboard layout.

```bash
  vim /etc/vconsole.conf
  KEYMAP=sv-latin1
```

Create and edit an hostname file. I'm naming the machine "vm"

```bash
  vim /etc/hostname
  vm
```

Now you are adding entries to your host file.

```bash
  vim /etc/hosts
  127.0.0.1	localhost
  ::1	    	localhost
  127.0.1.1	vm.localdomain	vm
```

Next you have to set your root password.

```bash
  passwd
```

### Bootloader

We are going to install and use grub as our bootloader.

```bash
  pacman -S grub
  grub-install --target=i386-pc /dev/sda
  grub-mkconfig -o /boot/grub/grub.cfg
```

Install dhcpcd as our dhcp client and reboot the machine.

```bash
  pacman -S dhcpcd
  systemctl enable dhcpcd
  exit
  reboot
```

Now you will see a login screen and the installation is complete.

![login](/images/archInstall/login.webp)

## User and Group creation

[Arch wiki](https://wiki.archlinux.org/index.php/Users_and_groups)  about user and group management.

You can create a user with a homefolder with following:

```bash
  useradd -m steve
```

Then you have to give that user a password.

```bash
  passwd steve
```

After that you have to give your user some groups

```bash
  usermod -aG wheel,audio,disk,optical,storage,video steve
```

## Sudo

To add a user to sudo you first need to install it.

```bash
  pacman -S Sudo
```

Then you need to change in the sudoers file.

```bash
  vim /etc/sudoers
```

Uncomment.
```bash
  %wheel ALL=(ALL) all
```

All user that belong to the wheel group now has admin rights.

Use sudo before your command to run it as a super user.

```bash
  sudo pacman -Syu
```