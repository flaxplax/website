---
title: "Docker"
date: 2021-04-10T10:05:28+02:00
lastmod: 2021-04-10T10:05:28+02:00
description: "docker documentation"
author: "Flaxplax"
draft: false
---

## Install
### Install with docker convenient script

This is the install script from the docker [docs](https://docs.docker.com/)

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

## Commands

```bash
docker run
# is used to run docker containers

docker run $image:$tag
# use a tag to run a specific version

docker run -d
# will run the container in detached mode
# the container will run in the background

docker run -it
# will make you have interactive mode with input to the container

docker run -p 80:8080
# will map port 80 to access the container with its internal port of 8080

docker run -v /opt/datadir:/var/lib/mysql mysql
# will mount the container data in a persistent volume

docker inspect $container_name
# will give you your container info

docker logs $container_name
# will show your docker logs

docker attach $docker_id
# will attach to a container running in the background

docker ps
# list containers and will give some basic information

docker ps -a
# list all running and exited containers

docker stop $container_name_or_id
# will stop container

docker rm $container_name_or_id
# will remove a container permanently

docker images
# will show docker images

docker rmi $image_or_id
# will remove the image
# you will have to remove a container using the image to remove the image

docker pull $image
# will download an docker image

docker exec $container cat /etc/hosts
# to execute a command in a docker container

docker system prune -a
# to delete all unused data
```

## Images

Here you can find sites to find docker images:


[linuxserver fleet](https://fleet.linuxserver.io/)

[docker hub](https://hub.docker.com/)