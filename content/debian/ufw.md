---
title: 'UFW'
date: '2024-04-23T21:28:49+02:00'
lastmod: '2024-04-23T21:28:49+02:00'
author: "Flaxplax"
description: "debian ufw documentation"
draft: false
---

## UFW

ufw is an uncomplicated firewall used to block and allow traffic

### Installation

You can install ufw with apt

```bash
apt install ufw
```

### Using ufw

To allow or deny ports you can use following commands

```bash
ufw allow 22 # to allow port 22

ufw deny 53 # to deny port 53
```

You can later start and enable ufw

```bash
ufw enable
```

To see status of ufw you can use the following command

```bash
ufw status

Status: active

To                         Action      From
--                         ------      ----
22                         ALLOW       Anywhere                  
53                         DENY        Anywhere                  
22 (v6)                    ALLOW       Anywhere (v6)             
53 (v6)                    DENY        Anywhere (v6)
```
