---
title: "ZFS"
date: 2021-08-23T19:49:54+02:00
lastmod: 2024-04-23T12:49:54+02:00
author: Flaxplax
draft: false
description: "Debian ZFS documentation"
---

## ZFS

ZFS is a copy on write filesystem.

### Installation

First you want to enable the `contrib` repo in `/etc/apt/sources.list

It should look like this

```bash
deb http://deb.debian.org/debian/ bookworm main non-free-firmware contrib
deb-src http://deb.debian.org/debian/ bookworm main non-free-firmware contrib
deb http://security.debian.org/debian-security bookworm-security main non-free-firmware contrib
deb-src http://security.debian.org/debian-security bookworm-security main non-free-firmware contrib
deb http://deb.debian.org/debian/ bookworm-updates main non-free-firmware contrib
deb-src http://deb.debian.org/debian/ bookworm-updates main non-free-firmware contrib
```

After that you can install zfs

```bash
apt update
apt install linux-headers-amd64 zfsutils-linux zfs-dkms zfs-zed
```

Now you can load the module and test it

```bash
modprobe zfs
zfs version

zfs-2.1.11-1
zfs-kmod-2.1.11-1
```

### zfs pool creation

You can create a zfs pool with the following command

```bash
zpool create pool /dev/sdx
```

You can show pool status with the command `zpool status`

```bash
zpool status
  pool: pool
 state: ONLINE
config:

	NAME        STATE     READ WRITE CKSUM
	pool        ONLINE       0     0     0
	 sdb       ONLINE       0     0     0

errors: No known data errors
```

### zfs datasets

You can create datasets for your pool with the command `zfs create <pool>/<dataset>`, and later list all datasets with the command `zfs list`

```bash
zfs create pool/my-dataset
zfs list
NAME              USED  AVAIL     REFER  MOUNTPOINT
pool              138K  30.5G       24K  /pool
pool/my-dataset    24K  30.5G       24K  /pool/my-dataset
```