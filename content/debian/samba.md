---
title: "Samba"
date: 2021-08-28T21:26:19+02:00
lastmod: 2021-08-28T21:26:19+02:00
author: Flaxplax
draft: false
description: "Debian samba documentation"
---

## Install Samba as DC

Install required packages, and run the configuration
```bash
apt -y install samba krb5-config winbind smbclient

 +----------------------+ Samba server and utilities +-----------------------+
 |                                                                           |
 | If your computer gets IP address information from a DHCP server on the    |
 | network, the DHCP server may also provide information about WINS servers  |
 | ("NetBIOS name servers") present on the network.  This requires a change  |
 | to your smb.conf file so that DHCP-provided WINS settings will            |
 | automatically be read from /var/lib/samba/dhcp.conf.                      |
 |                                                                           |
 | The dhcp-client package must be installed to take advantage of this       |
 | feature.                                                                  |
 |                                                                           |
 | Modify smb.conf to use WINS settings from DHCP?                           |
 |                                                                           |
 |                    <Yes>                       <No>                       |
 |                                                                           |
 +---------------------------------------------------------------------------+

 +------------------+ Configuring Kerberos Authentication +------------------+
 | When users attempt to use Kerberos and specify a principal or user name   |
 | without specifying what administrative Kerberos realm that principal      |
 | belongs to, the system appends the default realm.  The default realm may  |
 | also be used as the realm of a Kerberos service running on the local      |
 | machine.  Often, the default realm is the uppercase version of the local  |
 | DNS domain.                                                               |
 |                                                                           |
 | Default Kerberos version 5 realm:                                         |
 |                                                                           |
 | LOCAL.LAN________________________________________________________________ |
 |                                                                           |
 |                                  <Ok>                                     |
 |                                                                           |
 +---------------------------------------------------------------------------+

 +------------------+ Configuring Kerberos Authentication +------------------+
 | Enter the hostnames of Kerberos servers in the SRV.WORLD Kerberos         |
 | realm separated by spaces.                                                |
 |                                                                           |
 | Kerberos servers for your realm:                                          |
 |                                                                           |
 | smb.local.lan____________________________________________________________ |
 |                                                                           |
 |                                  <Ok>                                     |
 |                                                                           |
 +---------------------------------------------------------------------------+
 
 +------------------+ Configuring Kerberos Authentication +------------------+
 | Enter the hostname of the administrative (password changing) server for   |
 | the SRV.WORLD Kerberos realm.                                             |
 |                                                                           |
 | Administrative server for your Kerberos realm:                            |
 |                                                                           |
 | smb.local.lan____________________________________________________________ |
 |                                                                           |
 |                                  <Ok>                                     |
 |                                                                           |
 +---------------------------------------------------------------------------+

```

Then you should rename or remove the default config
```bash
mv /etc/samba/smb.conf /etc/samba/smb.conf.org
```

Set up the domain with the domain provision tool

```bash
samba-tool domain provision
Realm [SMB.LOCA.LAN]:  LOCAL.LAN
Domain [LOCAL]:  SMB01
Server Role (dc, member, standalone) [dc]:
DNS backend (SAMBA_INTERNAL, BIND9_FLATFILE, BIND9_DLZ, NONE) [SAMBA_INTERNAL]:
DNS forwarder IP address (write 'none' to disable forwarding) [192.168.50.1]:
Administrator password:
Retype password:

cp /var/lib/samba/private/krb5.conf /etc/
systemctl stop smbd nmbd winbind
systemctl disable smbd nmbd winbind 
systemctl unmask samba-ad-dc
systemctl start samba-ad-dc
systemctl enable samba-ad-dc 
```

Verify the status
```bash
smbclient -L localhost -U%
        Sharename       Type      Comment
        ---------       ----      -------
        sysvol          Disk
        netlogon        Disk
        IPC$            IPC       IPC Service (Samba 4.13.5-Debian)
```

Confirm the domain level
```bash
samba-tool domain level show

Domain and forest function level for domain 'DC=local,DC=lan'

Forest function level: (Windows) 2008 R2
Domain function level: (Windows) 2008 R2
Lowest function level of a DC: (Windows) 2008 R2
```