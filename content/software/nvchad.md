---
title: 'Nvchad'
date: '2025-02-01T21:36:37+01:00'
lastmod: '2025-02-01T21:36:37+01:00'
author: "Flaxplax"
description: ""
draft: false
---

## Nvchad

[nvchad github](https://github.com/NvChad/NvChad) 

### Installation

make sure you have neovim install and run the following command
```bash
git clone -b v2.0 https://github.com/NvChad/NvChad ~/.config/nvim --depth 1
```

### Theming

Use the keys `space + t + h` to open the menu to be abel to choose a theme

### Syntax highlight

use command to install syntax highlight for different languages like the following commands

```bash
:TSInstall bash
```

### File Tree

To open the file tree press `CTRL + n`

Mark a file by pressing the `m` key

Create a new file by pressing the `a` and giving the file a name

Copy files by using the `c` to copy and `p` to paste

Rename files by using the `r` key

### Telescope

Find and open project files by pressing `space + f +f`

Find and open current buffered files by pressing `space + f + b`

### Cheatsheet

To open cheatsheet press `space + c + h`

### Navigation

Navigate by using `CTRL + h/j/k/l`

Create more windows with following commands

```bash
:vsp # to split new window vertically
:sp # to split new window horizontally
:q # to exit window or neovim if no other windows is active
```

Toggle space numbers with the keys `space + n` or relative numbers with `space + r + n`

### Tabs

Change tab by pressing `tab` key or revers order by `shift + tab`

Close a tab by pressing `space + x`

### Terminal

Open a terminal by pressing `space + h` or `space + v` to have it horizontally
